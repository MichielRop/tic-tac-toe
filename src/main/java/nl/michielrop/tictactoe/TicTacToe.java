package nl.michielrop.tictactoe;

import java.util.List;
import java.util.Scanner;

import static nl.michielrop.tictactoe.Marker.M;

/**
 * $Id$
 */
public class TicTacToe {

    public static void main(String[] args) {
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {M, M, M},
                        {M, M, M},
                        {M, M, M}
                }), Marker.X);
        state.createChildren();
        state = state.miniMaxDecision();

        while(!state.isTerminal()){
            if (state.getPlayWith().equals(Marker.X)) {
                state = state.miniMaxDecision();
                System.out.println(state.toString());
            } else {
                final List<State> children = state.getChildren();
                int i = 0;
                for (State child : children) {
                    System.out.println(i);
                    System.out.println(child.toString());
                    System.out.println();
                    i++;
                }
                System.out.println("Please pick your move");
                Scanner sc = new Scanner(System.in, "UTF-8");
                String next = sc.nextLine();
                final int i1 = Integer.parseInt(next);
                state = children.get(i1);
            }
        }
        final double utility = state.utility();
        assert(utility!=Terminal.LOSS.getUtility());
        if (utility==Terminal.WIN.getUtility()){
            System.out.println("You lost !!!");
        }
        else{
            System.out.println("Draw....");
        }


    }
}
