package nl.michielrop.tictactoe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * $Id$
 */
public class State {

    public static final int SIZE = 3;

    private MarkerContainer[][] field = new MarkerContainer[SIZE][SIZE];
    private List<State> children=new ArrayList<State>();
    private Marker playWith;

    private Marker checkFor;

    public State(MarkerContainer[][] markerContainers, Marker playWith){
        this.playWith = playWith;
        this.field = copyMarkerContainers(markerContainers);
    }

    private MarkerContainer[][] copyMarkerContainers(MarkerContainer[][] markerContainers){
        MarkerContainer[][] result = new MarkerContainer[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                result[i][j]=new MarkerContainer(markerContainers[i][j].getMarker());
            }
        }
        return result;
    }

    public void createChildren() {
        for (int i = 0; i < getEmptyMarkers().size(); i++) {
            State state = new State(field,playWith.other());
            final List<MarkerContainer> emptyMarkers = state.getEmptyMarkers();
            MarkerContainer markerContainer = emptyMarkers.get(i);
            markerContainer.setMarker(playWith);
            if (!state.isTerminal()){
                state.createChildren();
            }
            children.add(state);
        }
    }

    private List<MarkerContainer> getEmptyMarkers() {
        List<MarkerContainer> emptyMarkers = new ArrayList<MarkerContainer>();
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (Marker.M.equals(field[i][j].getMarker())){
                    emptyMarkers.add(field[i][j]);
                }
            }
        }
        return emptyMarkers;
    }

    public boolean isTerminal(){
        return isFull() || wins(Marker.X) || wins(Marker.O);
    }


    private boolean isFull() {
        List<MarkerContainer> emptyMarkers = getEmptyMarkers();
        return emptyMarkers.size()==0;
    }

    public boolean wins(Marker marker){
        checkFor = marker;
        return check();
    }

    private boolean check() {
        Set<Marker> diagonalLeftToRight = new HashSet<Marker>();
        Set<Marker> diagonalRightToLeft = new HashSet<Marker>();
        for (int i = 0; i < SIZE; i++) {
            Set<Marker> row = new HashSet<Marker>();
            Set<Marker> column = new HashSet<Marker>();
            for (int j = 0; j < SIZE; j++) {
                row.add(field[i][j].getMarker());
                column.add(field[j][i].getMarker());
            }
            if (isWinning(row) || isWinning(column)){
                return true;
            }
            diagonalLeftToRight.add(field[i][i].getMarker());
            diagonalRightToLeft.add(field[SIZE-1-i][i].getMarker());
        }
        if (isWinning(diagonalLeftToRight)|| isWinning(diagonalRightToLeft)){
            return true;
        }
        return false;
    }

    private boolean isWinning(final Set<Marker> row) {
        return (row.size()==1) && row.contains(checkFor);
    }

    private MarkerContainer[][] copyMarkers(final MarkerContainer[][] field) {
        MarkerContainer[][] copy = new MarkerContainer[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                copy[i][j]=new MarkerContainer(field[i][j].getMarker());
            }
        }
        return copy;
    }

    public State miniMaxDecision(){
        double max=-1;
        State result=null;
        for (State child : children) {
            final double v = child.utility();
            if ( v>max){
                max = v;
                result = child;
            }
        }
        return result;
    }

    public double utility() {
        if (wins(Marker.X)){
            return Terminal.WIN.getUtility();
        }
        else if (wins(Marker.O)){
            return Terminal.LOSS.getUtility();
        }
        else if (isFull()){
            return Terminal.DRAW.getUtility();
        }
      else return miniMax();
    }

    private double miniMax() {
        if (playWith.equals(Marker.X)){
            double max =-1;
            for (State child : children) {
                final double v = child.miniMax();
                if (v>max){
                    max = v;
                }
            }
            return max;
        }
        else{
            double min = 1;
            for (State child : children) {
                final double v = child.miniMax();
                if (v<min){
                    min = v;
                }
            }
            return min;
        }
    }

    public List<State> getChildren() {
        return children;
    }

    public Marker getPlayWith() {
        return playWith;
    }

    public MarkerContainer[][] getField() {
        return copyMarkers(field);
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < SIZE; i++) {
            sb.append("-------\n");
            sb.append("|");
            for (int j = 0; j < SIZE; j++) {

                sb.append(field[i][j].toString());
                sb.append("|");
            }
            sb.append('\n');

        }
        sb.append("-------");
        return sb.toString();
    }


    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof State)) {
            return false;
        }
        final State state = (State) o;
        final MarkerContainer[][] field1 = state.getField();
        for (int i = 0; i <SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (!field1[i][j].getMarker().equals(field[i][j].getMarker())){
                    return false;
                }
            }
        }
        if (playWith != state.playWith) {
            return false;
        }
        return true;
    }



    @Override
    public int hashCode() {
        int result = playWith != null ? playWith.hashCode() : 0;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                result = 27 * result + field[i][j].hashCode();
            }
        }
        result = 31 * result;
        return result;
    }
}
