package nl.michielrop.tictactoe;

import nl.michielrop.tictactoe.Marker;
import nl.michielrop.tictactoe.MarkerContainer;
import nl.michielrop.tictactoe.State;

/**
 * $Id$
 */
public class MarkerContainerUtility {
    public static MarkerContainer[][] copyMarkers(Marker[][] markers){
        MarkerContainer[][] markerContainers = new MarkerContainer[State.SIZE][State.SIZE];
        for (int i = 0; i < State.SIZE; i++) {
            for (int j = 0; j < State.SIZE; j++) {
                markerContainers[i][j] = new MarkerContainer(markers[i][j]);
            }
        }
        return markerContainers;
    }
}
