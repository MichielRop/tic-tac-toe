package nl.michielrop.tictactoe;

/**
 * $Id$
 */
public class MarkerContainer {

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(final Marker marker) {
        this.marker = marker;
    }

    private Marker marker;

    public MarkerContainer(Marker marker){
        this.marker = marker;
    }

    public String toString(){
        return marker.toString();
    }

    public Marker other(){
        if (marker.equals(Marker.X)){
            return Marker.O;
        }
        else{
            return Marker.X;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MarkerContainer)) {
            return false;
        }

        final MarkerContainer that = (MarkerContainer) o;

        if (marker != that.marker) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return marker != null ? marker.hashCode() : 0;
    }
}
