package nl.michielrop.tictactoe;

/**
 * $Id$
 */
public enum Terminal {
    WIN(1),LOSS(-1),DRAW(0);
    private double utility;

    Terminal(final double utility) {
        this.utility = utility;
    }

    public double getUtility(){
        return this.utility;
    }
}
