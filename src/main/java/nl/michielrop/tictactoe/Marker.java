package nl.michielrop.tictactoe;

/**
 * $Id$
 */
public enum Marker {

    X("X"), O("O"), M(" ");

    @Override
    public String toString() {
        return s;
    }

    private String s;


    Marker(final String s) {
        this.s = s;
    }

    public Marker other(){
        if (this.equals(Marker.X)){
            return Marker.O;
        }
        else{
            return Marker.X;
        }
    }
}
