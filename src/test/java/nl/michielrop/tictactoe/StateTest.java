package nl.michielrop.tictactoe;

import java.util.Date;
import java.util.List;


import org.junit.Test;

import static nl.michielrop.tictactoe.Marker.X;
import static nl.michielrop.tictactoe.Marker.O;
import static nl.michielrop.tictactoe.Marker.M;

import junit.framework.Assert;

/**
 * $Id$
 */
public class StateTest {

    @Test
    public void testUtility_StateIsTerminal_Win(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, X, M},
                        {X, O, O}
                }),Marker.X);
        final double actual = state.utility();
        double expected = Terminal.WIN.getUtility();
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testUtility_StateIsTerminal_Lose(){
        State state = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, X},
                        {M, O, M}}),Marker.X);
        final double actual = state.utility();
        double expected = Terminal.LOSS.getUtility();
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testUtility_StateIsTerminal_Draw(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {O, O, X},
                        {X, X, O}
                }),Marker.X);
        final double actual = state.utility();
        double expected = Terminal.DRAW.getUtility();
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void createChildrenTest_putMarkerPossible(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {O, M, X},
                        {X, X, O}
                }),Marker.X);
        state.createChildren();
        final List<State> children = state.getChildren();
        State expected = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {O, X, X},
                        {X, X, O}
                }),Marker.O);
        final State actual = children.get(0);
        Assert.assertEquals(expected,actual);

    }

    @Test
    public void createChildrenTest_start(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {M, M, M},
                        {M, M, M},
                        {M, M, M}
                }),Marker.X);
        state.createChildren();
        final List<State> children = state.getChildren();
        final int actual = children.size();
        int expected = 9;
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void createChildrenTest_all(){
        long start = new Date().getTime();
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {M, M, M},
                        {M, M, M},
                        {M, M, M}
                }),Marker.X);
        state.createChildren();
        final List<State> children = state.getChildren();
        final State state1 = children.get(0);
        final int actual = state1.getChildren().size();
        int expected = 8;
        long end = new Date().getTime();
        long diff = end-start;
        System.out.println("Time passed:" + diff + " ms");

        //1719
        Assert.assertEquals(expected,actual);
    }


    @Test
    public void testIsTerminal_full_true(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {O, O, X},
                        {X, X, O}
                }),Marker.X);
        boolean expected = true;
        boolean actual = state.isTerminal();
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testIsTerminal_OWins_true(){
        State state = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, X},
                        {M, O, M}}),Marker.X);
        boolean expected = true;
        boolean actual = state.isTerminal();
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testIsTerminal_XWins_true(){
        State state = new State(MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, X, M},
                        {X, O, O}
                }),Marker.X);
        final boolean actual = state.isTerminal();
        final boolean expected = true;
        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testMiniMaxDecision_nextMoveCanWin_makeRightMove(){
        State state = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, X},
                        {M, M, M}}),Marker.X);
        state.createChildren();
        final State actual = state.miniMaxDecision();
        State expected = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, X},
                        {M, M, X}}),Marker.O);
        Assert.assertEquals(expected,actual);

    }

    @Test
    public void testMiniMaxDecision_afterNextMoveOCanWin_makeRightMove(){
        State state = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, M},
                        {M, M, M}}),Marker.X);
        state.createChildren();
        final State actual = state.miniMaxDecision();
        State expected = new State( MarkerContainerUtility.copyMarkers(
                new Marker[][]{
                        {X, O, X},
                        {M, O, M},
                        {M, X, M}}),Marker.O);
        Assert.assertEquals(expected,actual);
    }
}
